<?php

namespace Bus\Support;

use Bitrix\Main\Config\Option as OptionBx;
use Bitrix\Main\Context;
use Ds\Base\Helpers\Convert;
use Ds\Base\Helpers\File;
use Ds\Base\Helpers\Rating;
use Ds\Base\Helpers\User;

/**
 * Class Option
 * @package Ds\Base
 */
class Option
{
    const MODULE_ID = 'bus.support';

    /**
     * @param $name
     * @param string $default
     * @return string
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function get($name, $default='')
    {
        return OptionBx::get(self::MODULE_ID, $name, $default);
    }

    /**
     * @param $name
     * @param $value
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function set($name, $value)
    {
        OptionBx::set(self::MODULE_ID, $name, $value);
    }

    /**
     * @param $optionsAll
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \WheatleyWL\BXIBlockHelpers\Exceptions\IBlockHelperException
     */
    public static function saveAdminForm($optionsAll)
    {
        $request = Context::getCurrent()->getRequest();

        $jsonResult = [];
        if ($request->isPost()) {

            $saved = false;

            $token = $request->getPost('BUS_TOKEN');
            if (!empty($token)) {
                self::set('BUS_TOKEN', $token);
                $saved = true;
            }

            $login = $request->getPost('BUS_LOGIN');
            if (!empty($token)) {
                self::set('BUS_LOGIN', $login);
                $saved = true;
            }

            $pass = $request->getPost('BUS_PASSWORD');
            if (!empty($token)) {
                self::set('BUS_PASSWORD', $pass);
                $saved = true;
            }

            $url = $request->getPost('BUS_URL');
            if (!empty($token)) {
                self::set('BUS_URL', $url);
                $saved = true;
            }

            $userId = $request->getPost('BUS_USER_ID');
            if (!empty($token)) {
                self::set('BUS_USER_ID', $userId);
                $saved = true;
            }

            $serviceId = $request->getPost('BUS_SERVICE_ID');
            if (!empty($token)) {
                self::set('BUS_SERVICE_ID', $serviceId);
                $saved = true;
            }

            if ($saved) {
                global $APPLICATION;
                $APPLICATION->RestartBuffer();
            }
        }
    }
}