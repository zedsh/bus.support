<?php
namespace Bus\Support\Helpers;

//use Bus\Support\Helpers\BusService as BusService;

use Bus\Support\Exceptions\BusException;


/**
 * Class BusWorker
 *
 *
 * Работа с запросами waitProcessed ждет определенное кол-во времени задается (TIME_INTERVAL) и кол-во раз (REPEAT_COUNT)
 * возвращает результат со статусом отличным от 'wait'
 *
 * @package Bus\Support\Helpers
 */
class BusWorker
{
    const TIME_INTERVAL = 5;
    const REPEAT_COUNT = 6;
    protected $requestId;
    protected $response;

    /**
     * File constructor.
     * @param null $requestId
     * @param null $busAnswer
     */
    public function __construct($serviceCode, $data, $sync = false)
    {
        $this->busSerivce = BusService::getInstance();
        $this->response = $this->busSerivce->sendRequest($serviceCode, $data, $sync);
        $this->requestId = $this->response['requestId'];

    }

    public function  waitProcessed()
    {

        if (empty($this->requestId)) {
            return $this->response;

        }

        $repeatCount = self::REPEAT_COUNT;
        do {

            $this->responseRequset = $this->busSerivce->getRequest($this->requestId);

            if ($this->isError()) {
                throw new BusException($this->getError());
            }
            if ($this->isSuccess()) {
                return $this->responseRequset;
            }
            sleep(self::TIME_INTERVAL);
            $repeatCount--;
        } while($this->isWait() && $repeatCount>0);

        throw new BusException('Истекло время ожидания');
    }

    public function isError()
    {
        return ($this->responseRequset['status'] == 'error');
    }

    public function isWait()
    {
        return ($this->responseRequset['status'] == 'wait');
    }

    public function isSuccess()
    {
        return ($this->responseRequset['status'] == 'done');
    }

    public function  getData()
    {
        return $this->responseRequset['data'];
    }

    public function getError()
    {
        return $this->responseRequset['error'];
    }


}