<?php
namespace Bus\Support\Helpers;


use Bus\Support\Exceptions\BusException;
use Bus\Support\Option as OptionDs;
use GuzzleHttp\Client;

/**
 * Class BusService
 *
 * метод sendRequest отправляет запрос, при этом проходит авторизацию
 *
 * Метод getRequest возвразает id запроса
 *
 * Метод getContentsHandbook работает с справочниками
 *
 * @package Bus\Support\Helpers
 */
class BusService
{
    protected static $instance;

    protected $login;
    protected $password;
    protected $busUrl;
    protected $apiToken;
    protected $userToken;
    protected $client;

    protected function __construct()
    {
        $this->login = OptionDs::get('BUS_LOGIN','');
        $this->password = OptionDs::get('BUS_PASSWORD','');
        $this->apiToken = OptionDs::get('BUS_TOKEN','');
        $this->busUrl = OptionDs::get('BUS_URL','');

        //validation
        $this->client = new Client([
            'base_uri' => $this->busUrl,
            'timeout'  => 10.0,
        ]);


    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function isLoginPasswordPresent()
    {
        return (!empty($this->login) && !empty($this->password));
    }

    public function auth()
    {
        if(empty($this->apiToken) || ! $this->isLoginPasswordPresent()) {
            return null;
        }

        $userData = [
            'userToken' => null,
            'apiToken' => $this->apiToken,
            'dstService' => "bus",
            'sync' => true,
            'data' => [
                "method" => "auth",
                "email" => $this->login,
                "password" => $this->password,
            ],
        ];

        $response = $this->client->post('', ['json' => $userData]);
        $response = json_decode((string) $response->getBody()->getContents(), true);

        if (empty($response) || $response['status'] == 'error') {
            throw new BusException($response['error']['message']);
        }


        $this->userToken = $response['data']['userToken'];
    }

    public function sendRequest($serviceCode, $data, $sync = false)
    {
        if($this->isLoginPasswordPresent()) {
            $this->auth();
            $requestParameters = [
                'userToken' => $this->userToken,
                'apiToken' => $this->apiToken,
                'dstService' => $serviceCode,
                'sync' => $sync,
                'data' => $data,
            ];
        } else {
            $requestParameters = [
                'userToken' => null,
                'apiToken' => $this->apiToken,
                'dstService' => $serviceCode,
                'sync' => $sync,
                'data' => $data,
            ];
        }

        $response = $this->client->post('', ['json' => $requestParameters]);

        $response = json_decode($response->getBody()->getContents(), true);

        return $response;
    }

    public function getRequest($requestId)
    {
        $requestParameters = [
            'userToken' => $this->userToken,
            'apiToken' => $this->apiToken,
            'requestId' => $requestId,
        ];

        $response = $this->client->post('', ['json' => $requestParameters]);
        $response = json_decode($response->getBody()->getContents(), true);

        return $response;
    }

    public function getContentsHandbook($method, $codeHandbook, $limit='', $offset='')
    {
        if (empty($codeHandbook)) {
            throw new BusException('не указан код сравочника');
        }
        if (empty($method)) {
            throw new BusException('не указан метод сравочника');
        }

        $data = [
            'method' => $method,
            'directory' => $codeHandbook,
        ];
        if (!empty($limit)) {
            $data['pagination']['limit'] = $limit;
        }
        if (!empty($offset)) {
            $data['pagination']['offset'] = $limit;
        }
        print '<pre>' . print_r($data, true) . '</pre>';

        $nsi = new BusWorker('nsi', $data);
        $response = $nsi->waitProcessed();

        return $response['data'];

    }

}