<?php
namespace Bus\Support\Helpers;

use Bus\Support\Exceptions\BusException;
use Bus\Support\Option as OptionDs;
use GuzzleHttp\Client;

/**
 * Class BusRequestProcessor
 *
 *принимает запросы от шины getData - возвращает данные
 *
 * returnResponse - ответ шине
 *
 * @package Bus\Support\Helpers
 */
class BusRequestProcessor
{
    public static function getData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $users = self::getUserId();
        $services = self::getServiceId();

        if (!empty($users) && strpos($users, $data['userId']) === false) {
            throw new BusException('Нет доступа указанному пользователю');
        }

        if (!empty($users) && strpos($services, $data['srcService']) === false) {
            throw new BusException('Нет доступа указанному сервису');
        }

        return $data['data'];
    }

    protected static function getUserId()
    {
       return OptionDs::get('BUS_USER_ID','');
    }

    protected static function getServiceId()
    {
        return OptionDs::get('BUS_SERVICE_ID','');
    }

    protected static function getClient()
    {
        $client = new Client([
            'base_uri' => OptionDs::get('BUS_URL',''),
            'timeout'  => 10.0,
        ]);
        return $client;
    }


    public static function returnResponse($data)
    {
	global $APPLICATION;
	$APPLICATION->RestartBuffer();
        echo json_encode($data);
	die();
    }
}
