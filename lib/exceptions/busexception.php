<?php
namespace Bus\Support\Exceptions;

/**
 * Class AjaxComponentValidationException
 *
 * @package Bus\Support\Exceptions
 */
class BusException extends \Exception
{
}