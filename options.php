<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;

use Bus\Support\Option as OptionDs;
use Bus\Support\Helpers\BusService as BusService;
use Bus\Support\Helpers\BusWorker as BusWorker;

Loader::includeModule('bus.support');

$shinaKeys = [
    'BUS_TOKEN' => [
        'default' => '',
    ],
    'BUS_LOGIN' => [
        'default' => '',
    ],
    'BUS_PASS' => [
        'default' => '',
    ],

];

$optionsAll = [
    'shinaKeys' => $shinaKeys,
];

OptionDs::saveAdminForm($optionsAll);

$aTabs = array();
$aTabs[] = array("DIV"=>"main", "TAB"=>'Основные', "ICON"=>"main_edit", "TITLE"=>'Рейтинги экспертов');

$editTab = new CAdminTabControl("editTab", $aTabs, true, true);
?>
<form id="ds-shina-form" method="post" action="<?=POST_FORM_ACTION_URI?>">
    <?php
    $editTab->Begin();
    $editTab->BeginNextTab();
    ?>
    <tr>
        <td>
            <div id="inspections" class="adm-detail-block">
                <table cellspacing="2" cellpadding="3">
                    <tr>
                        <th>Токен системы</th>
                        <td>
                            <input type="text" style="text-align: left" size="30"  name="BUS_TOKEN" value="<?=OptionDs::get('BUS_TOKEN')?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Логин пользователя</th>
                        <td>
                            <input type="text" style="text-align: left" size="30"  name="BUS_LOGIN" value="<?=OptionDs::get('BUS_LOGIN')?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Пароль пользователя</th>
                        <td>
                            <input type="text" style="text-align: left" size="30"  name="BUS_PASSWORD" value="<?=OptionDs::get('BUS_PASSWORD')?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th>url Шины</th>
                        <td>
                            <input type="text" style="text-align: left" size="30"  name="BUS_URL" value="<?=OptionDs::get('BUS_URL')?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th>ID пользователей (через  запятую)</th>
                        <td>
                            <input type="text" style="text-align: left" size="30"  name="BUS_USER_ID" value="<?=OptionDs::get('BUS_USER_ID')?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Коды систем (через запятую)</th>
                        <td>
                            <input type="text" style="text-align: left" size="30"  name="BUS_SERVICE_ID" value="<?=OptionDs::get('BUS_SERVICE_ID')?>"/>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>

    <?php
    $editTab->Buttons([
        'btnSave' => false,
        'btnApply' => 1,
    ]);
    $editTab->End();
    ?>

</form>
