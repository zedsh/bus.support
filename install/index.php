<?php
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);
/**
 * Class bus_support
 */
class bus_support extends CModule
{
    public $MODULE_ID;

    public $MODULE_NAME;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public $MODULE_GROUP_RIGHTS = "Y";

    /**
     * ds_base constructor.
     */
    public function __construct()
    {
        /**
         * @global array $arModuleVersion
         */
        include(__DIR__ . "/version.php");
        $this->MODULE_ID = str_replace('_', '.',__CLASS__);
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("DS_BUS_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("DS_BUS_MODULE_DESCRIPTION");
        $this->PARTNER_NAME = Loc::getMessage("DS_BUS_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("DS_BUS_PARTNER_URI");
    }

    /**
     *
     */
    public function DoInstall()
    {
        RegisterModule($this->MODULE_ID);

    }

    /**
     * 
     */
    public function DoUninstall()
    {

        UnRegisterModule($this->MODULE_ID);
    }

}
